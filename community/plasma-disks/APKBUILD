# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-disks
pkgver=5.27.7
pkgrel=1
pkgdesc="Monitors S.M.A.R.T. capable devices for imminent failure"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	kirigami2
	smartmontools
	"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kcoreaddons-dev
	kdbusaddons-dev
	knotifications-dev
	ki18n-dev
	solid-dev
	kservice-dev
	kio-dev
	kauth-dev
	kdeclarative-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-disks-$pkgver.tar.xz"
subpackages="$pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-disks.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e4b42beb5d17019f6d664bfcebc2be78c422e6a65d944b491183a2ae65bc701af0b46c70472ca624691d86c1833c33a47485bf786d3d723945f641ea4e8eb3f0  plasma-disks-5.27.7.tar.xz
"
