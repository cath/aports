# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-integration
pkgver=5.27.7
pkgrel=1
pkgdesc="Qt Platform Theme integration plugins for the Plasma workspaces"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.1-only AND ((LGPL-2.1-only WITH Nokia-Qt-exception-1.1) OR (GPL-3.0-only WITH Nokia-Qt-exception-1.1))"
depends="
	font-hack
	font-noto
	qqc2-desktop-style
	"
makedepends="
	breeze-dev
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libxcursor-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-integration-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-integration.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontNotoSans=true \
		-DCMAKE_DISABLE_FIND_PACKAGE_FontHack=true
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
d24b5d73e6e86967b0b2435e082c3be1433c161c20826ccc68d08777a1acb2d606c3afa89bf2f097f393448bb66d50b93eb0631790fd0990bd270306239a6ac6  plasma-integration-5.27.7.tar.xz
"
