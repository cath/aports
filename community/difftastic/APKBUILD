# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=difftastic
pkgver=0.49.0
pkgrel=0
pkgdesc="Diff tool that understands syntax"
url="https://difftastic.wilfred.me.uk/"
license="MIT"
arch="all !s390x !riscv64" # blocked by rust/cargo
makedepends="cargo openssl-dev cargo-auditable"
source="https://github.com/Wilfred/difftastic/archive/$pkgver/difftastic-$pkgver.tar.gz"


prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/difft -t "$pkgdir"/usr/bin/
}

sha512sums="
f4ea97a67a828128e9d41ec33d8ae5a21b0afdc25bd0a58e12bb70d6d739dd084e3b94eeb477d27f8b59943213ff98798319f74fe06e5bd5e9c911df918727c5  difftastic-0.49.0.tar.gz
"
