# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=1.35.8
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	git init .
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin rtx
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rtx \
		-t "$pkgdir"/usr/bin/
}

sha512sums="
dcab743316efa468ea7a9562ae390555766b09c6469b68e2aa11531e2a51821e8e40b9b5e11f833df757e5fa64d0c50066bd91fb739ffc22297ecac537220c2c  rtx-1.35.8.tar.gz
"
