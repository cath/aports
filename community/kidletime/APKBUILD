# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kidletime
pkgver=5.108.0
pkgrel=2
pkgdesc="Monitoring user activity"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only"
makedepends="
	doxygen
	extra-cmake-modules
	plasma-wayland-protocols
	qt5-qttools-dev
	qt5-qtwayland-dev
	qt5-qtx11extras-dev
	samurai
	wayland-dev
	wayland-protocols
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kidletime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kidletime.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# solidmttest is broken
	ctest --test-dir build --output-on-failure -E "solidmttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
48972772548b8b81fcee7d06537cddc158c913d77aac7a233200eb29d17f7f6e1d2d314c60b97bf1c215b5a544c0ba6c58521b2dbdb827ac2cda950356a9cbfd  kidletime-5.108.0.tar.xz
"
