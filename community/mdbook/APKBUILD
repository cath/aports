# Maintainer: crapStone <crapstone01@gmail.com>
pkgname=mdbook
pkgver=0.4.33
pkgrel=0
pkgdesc="mdBook is a utility to create modern online books from Markdown files"
url="https://rust-lang.github.io/mdBook/"
arch="all"
license="MPL-2.0"
makedepends="rust cargo cargo-auditable"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rust-lang/mdBook/archive/v$pkgver.tar.gz"
builddir="$srcdir/mdBook-$pkgver"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release

	./target/release/mdbook completions bash > $pkgname.bash
	./target/release/mdbook completions fish > $pkgname.fish
	./target/release/mdbook completions zsh > $pkgname.zsh
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/mdbook "$pkgdir"/usr/bin/mdbook

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
d2693600da5822961dfe52f82e6b64ce663f086feff419527f1fdceb091099ffab963ba395535bc36eb348ed6ad16806d8bc4f2ce272f42110d464d500ef5205  mdbook-0.4.33.tar.gz
"
