# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kross
pkgver=5.108.0
pkgrel=2
pkgdesc="Framework for scripting KDE applications"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcompletion-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kross-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kross.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
577722bb280dfe7084fd70866297cea2a34c9f762fe98744a61e92bcb0ee12e4e5852d79a651f0121180641df4ff6b1ff995a8af71ad5ee361444f8c0aea4bad  kross-5.108.0.tar.xz
"
