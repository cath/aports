# Maintainer: Wesley van Tilburg <justwesley@protonmail.com>
pkgname=minify
pkgver=2.12.8
pkgrel=1
pkgdesc="Minifier CLI for HTML, CSS, JS, JSON, SVG and XML"
url="https://github.com/tdewolff/minify"
arch="all"
options="net"
license="MIT"
makedepends="go bash"
subpackages="$pkgname-bash-completion"
source="$pkgname-$pkgver.tar.gz::https://github.com/tdewolff/minify/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	mkdir build
	go build -o build ./cmd/minify
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./build/minify -t "$pkgdir"/usr/bin
	install -Dm644 ./cmd/minify/bash_completion "$pkgdir"/usr/share/bash-completion/completions/minify
}

sha512sums="
dc46ee54544bee1767f0300692cbc6cd3c805b8f7a54cac8692a0ec64038130db370d98f776b3c7d7458699345f24e16d62984293da4a60b1fe37e0d95d177b5  minify-2.12.8.tar.gz
"
