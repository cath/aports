# Contributor: Thomas Deutsch <thomas@tuxpeople.org>
# Maintainer: Thomas Deutsch <thomas@tuxpeople.org>
pkgname=lazygit
pkgver=0.40.0
pkgrel=0
pkgdesc="Simple terminal UI for git commands"
url="https://github.com/jesseduffield/lazygit"
arch="all"
license="MIT"
# bash: for running git commands
depends="ncurses bash"
makedepends="go"
options="!check" # FIXME: https://github.com/jesseduffield/lazygit/issues/1009
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jesseduffield/lazygit/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags="-X main.version=$pkgver" -v
}

package() {
	install -Dm0755 $pkgname -t "$pkgdir"/usr/bin

	mkdir -p "$pkgdir"/usr/share/doc
	mv docs "$pkgdir"/usr/share/doc/"$pkgname"
	install -Dm0644 LICENSE -t "$pkgdir"/usr/share/licenses/"$pkgname"
}

sha512sums="
39b3cf0a1f49a5fb32e60bfe5a7af9ec867987a06145b24ac8061e8e2a62c620f0683313b18987bed4de5d431b344ea9b088b742fb6754a27750e4dd4c660336  lazygit-0.40.0.tar.gz
"
