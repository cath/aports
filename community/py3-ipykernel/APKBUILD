# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-ipykernel
pkgver=6.25.0
pkgrel=0
pkgdesc="IPython kernel for jupyter"
url="https://github.com/ipython/ipykernel"
arch="noarch"
license="BSD-3-Clause"
depends="
	ipython
	py3-comm
	py3-jupyter_client
	py3-jupyter_core
	py3-matplotlib-inline
	py3-nest_asyncio
	py3-packaging
	py3-psutil
	py3-pyzmq
	py3-tornado
	py3-traitlets
	"
makedepends="py3-gpep517 py3-hatchling"
checkdepends="py3-ipyparallel py3-pytest py3-flaky py3-pytest-timeout py3-pytest-asyncio"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/ipython/ipykernel/releases/download/v$pkgver/ipykernel-$pkgver.tar.gz
	deprecation-warnings.patch"
builddir="$srcdir/ipykernel-$pkgver"
options="!check" # py3-ipyparallel is circular, and an optional dep

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest \
		--deselect ipykernel/tests/test_kernel.py::test_shutdown_subprocesses
}

package() {
	python3 -m installer --destdir="$pkgdir" .dist/*.whl
}

sha512sums="
7cd2d3cbb2c83ca165798dc830aae096ce3391535ef604f257c4c79357ebd1c7f7e95c6952e1fb95d1a460ccb5b1bf88396f984b6462986b3e73a4453dd33b52  py3-ipykernel-6.25.0.tar.gz
5e1ccda828995dabc4e156dcb8e3f53bd99dcdfff6d109e9278d7daecd63db7b7973d5652ff68c180d7147d54cb42b66841e5bca0560078d38b698d93995be4e  deprecation-warnings.patch
"
