# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=plasma-framework
pkgver=5.108.0
pkgrel=2
pkgdesc="Plasma library and runtime components based upon KF5 and Qt5"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND GPL-2.0-or-later"
depends_dev="
	kactivities-dev
	karchive-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kirigami2-dev
	knotifications-dev
	kpackage-dev
	kservice-dev
	kwayland-dev
	kwindowsystem-dev
	kxmlgui-dev
	mesa-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="
	hicolor-icon-theme
	xvfb-run
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/plasma-framework-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # 8 out of 13 tests fail

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/plasma-framework.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
58b69b8c99d63dc96a82be852314dfe6d000b51a630c15927436911e2fe6fe842c0b5b4b17dbfa9bb985cd98fcdae772f22e5571ec610472fd1f856417194b3e  plasma-framework-5.108.0.tar.xz
"
