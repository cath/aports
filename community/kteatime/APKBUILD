# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kteatime
pkgver=23.04.3
pkgrel=1
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.kteatime"
pkgdesc="A handy timer for steeping tea"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="extra-cmake-modules qt5-qtbase-dev kconfig-dev kcrash-dev kdoctools-dev ki18n-dev kiconthemes-dev knotifyconfig-dev knotifications-dev ktextwidgets-dev kxmlgui-dev kconfig-dev samurai"
source="https://download.kde.org/stable/release-service/$pkgver/src/kteatime-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
73eb8ff318a6bd51acce1292d344e19069809b4608a3d40d26cd6bb630e6b49989539504c323cd98917711c418977f4760ec3e4d0499ee5ac71b7d8732c13600  kteatime-23.04.3.tar.xz
"
