# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=cmake
pkgver=3.27.1
pkgrel=0
pkgdesc="Cross-platform, open-source make system"
url="https://www.cmake.org/"
arch="all"
license="BSD-3-Clause"
makedepends="
	bzip2-dev
	curl-dev
	expat-dev
	libarchive-dev
	libuv-dev
	linux-headers
	ncurses-dev
	py3-sphinx
	rhash-dev
	samurai
	xz-dev
	zlib-dev
	"
checkdepends="file"
subpackages="
	ccmake
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-emacs::noarch
	$pkgname-vim::noarch
	"
case $pkgver in
*.*.*.*) _v=v${pkgver%.*.*};;
*.*.*) _v=v${pkgver%.*};;
esac
source="https://www.cmake.org/files/$_v/cmake-$pkgver.tar.gz"
options="!check"

build() {
	# jsoncpp/cppdap needs cmake to build so to avoid recursive build
	# dependency, we use the bundled version.
	# Do NOT remove --no-system-jsoncpp or --no-system-cppdap

	./bootstrap \
		--prefix=/usr \
		--mandir=/share/man \
		--datadir=/share/$pkgname \
		--docdir=/share/doc/$pkgname \
		--sphinx-man \
		--no-system-cppdap \
		--no-system-jsoncpp \
		--system-bzip2 \
		--system-curl \
		--system-expat \
		--system-libarchive \
		--system-liblzma \
		--system-librhash \
		--system-libuv \
		--system-nghttp2 \
		--system-zlib \
		--generator=Ninja \
		--parallel="${JOBS:-2}"
	ninja
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE bin/ctest
}

package() {
	DESTDIR="$pkgdir" ninja install
}

ccmake() {
	pkgdesc="$pkgdesc (ccmake configurator)"

	amove usr/bin/ccmake
}

emacs() {
	pkgdesc="$pkgdesc (emacs mode)"
	install_if="$pkgname=$pkgver-r$pkgrel emacs"

	amove usr/share/emacs
}

vim() {
	pkgdesc="$pkgdesc (vim files)"
	install_if="$pkgname=$pkgver-r$pkgrel vim"

	amove usr/share/vim
}

sha512sums="
d8831105fed3772bcb821a2b184fe0ffbff15795a5b0a89f2ad1cb0372a40c8f22f2bab72c671a6d17464521b7337067df929a5c3ece99f84848cc3a2fe6716f  cmake-3.27.1.tar.gz
"
