# Maintainer: Catherine Schönhammer <cath@schoenha.com>

_flavor=surface-lts
pkgname=linux-$_flavor
pkgver=6.1.43
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=0
pkgdesc="Linux lts kernel for Surface Devices"
url="https://www.kernel.org"
depends="initramfs-generator"
_depends_dev="perl gmp-dev mpc1-dev mpfr-dev elfutils-dev bash flex bison zstd"
makedepends="$_depends_dev sed installkernel bc linux-headers linux-firmware-any openssl-dev>3 mawk
	diffutils findutils zstd pahole python3 gcc>=13.1.1_git20230624"
options="!strip"
_config=${config:-config-lts.${CARCH}}
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz

	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0001-surface3-oemb.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0002-mwifiex.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0003-ath10k.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0004-ipts.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0005-ithc.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0006-surface-sam.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0007-surface-sam-over-hid.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0008-surface-button.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0009-surface-typecover.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0010-surface-shutdown.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0011-surface-gpe.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0012-cameras.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0013-amd-gpio.patch
	https://raw.githubusercontent.com/linux-surface/linux-surface/master/patches/${_kernver}/0014-rtc.patch

	0001-powerpc-config-defang-gcc-check-for-stack-protector-.patch
	0001-x86-Compress-vmlinux-with-zstd-19-instead-of-22.patch
	0001-kexec-add-kexec_load_disabled-boot-option.patch
	awk.patch
	gcc13.patch
	ppc-notext.patch
	0001-tty-Move-sysctl-setup-into-core-tty-logic.patch
	0002-tty-Allow-TIOCSTI-to-be-disabled.patch
	0003-tty-Move-TIOCSTI-toggle-variable-before-kerndoc.patch

	lts.aarch64.config
	lts.armv7.config
	lts.x86.config
	surface-lts.x86_64.config
	lts.ppc64le.config
	lts.s390x.config

	virt.aarch64.config
	virt.armv7.config
	virt.ppc64le.config
	virt.x86.config
	"
subpackages="$pkgname-dev:_dev:$CBUILD_ARCH $pkgname-doc"
for _i in $source; do
	case $_i in
	*.$CARCH.config)
		_f=${_i%."$CARCH".config}
		_flavors="$_flavors $_f"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-$_f::$CBUILD_ARCH linux-$_f-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done
builddir="$srcdir"/linux-$_kernver

if [ "${pkgver%.0}" = "$pkgver" ]; then
	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz"
fi
arch="all !armhf !riscv64"
license="GPL-2.0-only"

# secfixes:
#   6.1.27-r3:
#     - CVE-2023-32233
#   5.10.4-r0:
#     - CVE-2020-29568
#     - CVE-2020-29569
#   5.15.74-r0:
#     - CVE-2022-41674
#     - CVE-2022-42719
#     - CVE-2022-42720
#     - CVE-2022-42721
#     - CVE-2022-42722

prepare() {
	if [ "$_kernver" != "$pkgver" ]; then
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N
	fi

	default_prepare

	# remove localversion from patch if any
	rm -f localversion*
}

_kernelarch() {
	local arch="$1"
	case "$arch" in
		aarch64*) arch="arm64" ;;
		arm*) arch="arm" ;;
		mips*) arch="mips" ;;
		ppc*) arch="powerpc" ;;
		s390*) arch="s390" ;;
	esac
	echo "$arch"
}

_prepareconfig() {
	local _flavor="$1"
	local _arch="$2"
	local _config=$_flavor.$_arch.config
	local _builddir="$srcdir"/build-$_flavor.$_arch
	mkdir -p "$_builddir"
	echo "-$pkgrel-$_flavor" > "$_builddir"/localversion-alpine

	cp "$srcdir"/$_config "$_builddir"/.config
	msg "Configuring $_flavor kernel ($_arch)"
	make -C "$srcdir"/linux-$_kernver \
		O="$_builddir" \
		ARCH="$(_kernelarch $_arch)" \
		olddefconfig

	if grep "CONFIG_MODULE_SIG=y" "$_builddir"/.config >/dev/null; then
		if [ -f "$KERNEL_SIGNING_KEY" ]; then
			sed -i -e "s:^CONFIG_MODULE_SIG_KEY=.*:CONFIG_MODULE_SIG_KEY=\"$KERNEL_SIGNING_KEY\":" \
				"$_builddir"/.config
			msg "Using $KERNEL_SIGNING_KEY to sign $_flavor kernel ($_arch) modules"
		else
			warning "KERNEL_SIGNING_KEY was not set. A signing key will be generated, but 3rd"
			warning "party modules can not be signed"
		fi
	fi
}

listconfigs() {
	for i in $source; do
		case "$i" in
			*.config) echo $i;;
		esac
	done
}

prepareconfigs() {
	for _config in $(listconfigs); do
		local _flavor=${_config%%.*}
		local _arch=${_config%.config}
		_arch=${_arch#*.}
		local _builddir="$srcdir"/build-$_flavor.$_arch
		_prepareconfig "$_flavor" "$_arch"
	done
}

# this is supposed to be run before version is bumped so we can compare
# what new kernel config knobs are introduced
prepareupdate() {
	clean && fetch && unpack && prepare && deps
	prepareconfigs
	rm -r "$srcdir"/linux-$_kernver
}

updateconfigs() {
	if ! [ -d "$srcdir"/linux-$_kernver ]; then
		deps && fetch && unpack && prepare
	fi
	for _config in ${CONFIGS:-$(listconfigs)}; do
		msg "updating $_config"
		local _flavor=${_config%%.*}
		local _arch=${_config%.config}
		_arch=${_arch#*.}
		local _builddir="$srcdir"/build-$_flavor.$_arch
		mkdir -p "$_builddir"
		echo "-$pkgrel-$_flavor" > "$_builddir"/localversion-alpine
		local actions="listnewconfig oldconfig"
		if ! [ -f "$_builddir"/.config ]; then
			cp "$srcdir"/$_config "$_builddir"/.config
			actions="olddefconfig"
		fi
		env | grep ^CONFIG_ >> "$_builddir"/.config || true
		make -j1 -C "$srcdir"/linux-$_kernver \
			O="$_builddir" \
			ARCH="$(_kernelarch $_arch)" \
			$actions savedefconfig

		cp "$_builddir"/defconfig "$startdir"/$_config
	done
}

build() {
	unset LDFLAGS
	# for some reason these sometimes leak into the kernel build,
	# -Werror=format-security breaks some stuff
	unset CFLAGS CPPFLAGS CXXFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		_prepareconfig "$i" "$CARCH"
	done
	for i in $_flavors; do
		msg "Building $i kernel"
		cd "$srcdir"/build-$i.$CARCH

		# set org in cert for modules signing
		# https://www.kernel.org/doc/html/v6.1/admin-guide/module-signing.html#generating-signing-keys
		mkdir -p certs
		sed -e 's/#O = Unspecified company/O = alpinelinux.org/' \
			"$srcdir"/linux-$_kernver/certs/default_x509.genkey \
			> certs/x509.genkey

		make ARCH="$(_kernelarch $CARCH)" \
			CC="${CC:-gcc}" \
			AWK="${AWK:-mawk}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$srcdir"/build-$_buildflavor.$CARCH
	local _abi_release="$(make -s kernelrelease)"
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		*) _install=install;;
	esac

	make modules_install $_install \
		ARCH="$(_kernelarch $CARCH)" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/"$_abi_release"/build \
		"$_outdir"/lib/modules/"$_abi_release"/source
	rm -rf "$_outdir"/lib/firmware

	install -D -m644 include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package "$_flavor" "$pkgdir"

	# copy files for linux-lts-doc sub package
	mkdir -p "$pkgdir"/usr/share/doc
	cp -r "$srcdir"/linux-"$_kernver"/Documentation \
		"$pkgdir"/usr/share/doc/linux-doc-"$pkgver"/
	# remove files that aren't part of the documentation itself
	for nondoc in \
		.gitignore conf.py docutils.conf \
		dontdiff Kconfig Makefile
	do
		rm "$pkgdir"/usr/share/doc/linux-doc-"$pkgver"/"$nondoc"
	done
	# create /usr/share/doc/linux-doc symlink
	cd "$pkgdir"/usr/share/doc; ln -s linux-doc-"$pkgver" linux-doc
}

# subflavors install in $subpkgdir
virt() {
	_package virt "$subpkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _builddir="$srcdir"/build-$_flavor.$CARCH
	local _abi_release="$(make -C "$_builddir" -s kernelrelease)"
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-"$_abi_release"
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp -a "$_builddir"/.config "$_builddir"/localversion-alpine \
		"$dir"/

	install -D -t "$dir"/certs "$_builddir"/certs/signing_key.x509 || :

	make -C "$srcdir"/linux-$_kernver \
		O="$dir" \
		ARCH="$(_kernelarch $CARCH)" \
		AWK="${AWK:-mawk}" \
		prepare modules_prepare scripts

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the sources
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk
	cd "$srcdir"/linux-$_kernver
	find .  -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' -o -name 'Platform' \) \
		-print | cpio -pdm "$dir"

	cp -a scripts include "$dir"

	find "arch/$_karch" -name include -type d -print | while IFS='' read -r folder; do
		find "$folder" -type f
	done | sort -u | cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build-$_flavor.$CARCH/Module.symvers \
		"$dir"/Module.symvers

	# remove unneeded things
	msg "Removing documentation..."
	rm -r "$dir"/Documentation
	find "$dir" -type f -name '*.o' -printf 'Removing %P\n' -delete
	local _karch="$(_kernelarch $CARCH | sed 's/x86_64/x86/')"
	msg "Removing unneeded arch headers..."
	for i in "$dir"/arch/*; do
		if [ "${i##*/}" != "$_karch" ]; then
			echo "  ${i##*/}"
			rm -r "$i"
		fi
	done

	mkdir -p "$subpkgdir"/lib/modules/"$_abi_release"
	ln -sf /usr/src/linux-headers-"$_abi_release" \
		"$subpkgdir"/lib/modules/"$_abi_release"/build
}

sha512sums="
6ed2a73c2699d0810e54753715635736fc370288ad5ce95c594f2379959b0e418665cd71bc512a0273fe226fe90074d8b10d14c209080a6466498417a4fdda68  linux-6.1.tar.xz
538ded3e6a6cba2a986cf9557f4ac4b754f72ba33bba25b57bafed35de376f2ca323c8de28db03ed8ce5edc5a6dd787766ab2c8dc49b45d489cb8f32bda11dcd  0001-surface3-oemb.patch
aecd5f4438d0904ae80ea8babcb0dccc2260bc5805796dfbe101da3b16ca9e61c2707b90d6a99b57a9bf3a8270ebf904709c9a112ec86db81a4db3f0b04cc550  0002-mwifiex.patch
88217ca738bda94c0c1bde868560d70cf9c76a292adb0fdecb5035e3834edeb3d8485a29a17ac209656121ef22fc5eb4e1e7d46b22abe2dbc3ada077b64b300b  0003-ath10k.patch
cf7f68ce7794fafddcc4fcbbbeb17199df58f6f1fc22145ae9e857d67b0be74d66bc4af3df14d73497983238bc0477d1ba61a166df3a18e39a0e58688338e3c3  0004-ipts.patch
9bebf0989dee4c10e19ddfb8ad429668d2e046e6bdd59dd0c0781116ea11f4f05f3aeae1b1fa3255aeb95150e80a4dfd1300321492951ced1ef259f895b4f942  0005-ithc.patch
a6841ba3627abd53cd6484ba485b4acd25f6fb435d08f6d6281fc2e3ca9aa17e556834f79dcb5bcf858b53dcd2afc0b8477b6bcc4db168fbd75c79bde4a761ca  0006-surface-sam.patch
28d7b4dfc62c121c1c0dbb7b2a29cdf9bf5c0e1ad926ed80ef7666abf80cfa749ca26d04c419d41018c94ab8643b4bc403907ad0208e708154c8ff0b3e7d5d61  0007-surface-sam-over-hid.patch
1345d1b88b353402d6134c0cefae28627dfe7c278a41f0023f4de7c51151b4f7d80859d288f0c2cf1b5255b91f6801016eb026b064318acaa6bb441afe56ee6c  0008-surface-button.patch
263908056ce1f1ff08eb7d7a9bed9359b9c2d05b9406ff7174fd106e594ec6387ad1de7ec966c604174bcc9a20119bfc5d5cc5701ab192dd881e4ad85a6b1ad6  0009-surface-typecover.patch
aed21bde5d0c6d9ecef0f30d443de370937ff4a206c9f46f83665f2133448b6f1f7cfbb5f21e6a24d17b260acb344fd738c6ec1dd30dfa317097b74d471a186b  0010-surface-shutdown.patch
2342eae6804576e3bde6dd971f16eda4055d5b772bb2eb54e2c9f6345d9b3e53af22e7209d0279ea6a404f0a3078a294c5aaadd6d53e778c8c057fd94ef91424  0011-surface-gpe.patch
7af6c5c68717e9b67450f9fd9139ca4d54d94d4e7203bd64de776f2748bc0b773b90305ea2ba4f2430448fd52e5135285e0fecfe8e26215e3adc1f0fa872d16d  0012-cameras.patch
aaac4ea635d078fc98d1d4f90a826ee528e48e0f745f8557c3177397c3052896e76061d42923e4cc6b50902f1367219aadf03b2da85b573916bf0f5e73a5f3a3  0013-amd-gpio.patch
30b90754f91d5c63df2c733d27f64dc76fe3466506b39d9413e7dc46ecd5ef6ba0331029f6fb04ee2ee7a06068aea98b9d5ab566d2ebe37cee66c8946401f648  0014-rtc.patch
a6c826e8c3884550fbe1dee3771b201b3395b073369f7fbfb5dc4aa5501e848b0bbc3a5ad80d932a6a50cbe0cfd579c75662f0c43ef79a38c1c6848db395c4c8  0001-powerpc-config-defang-gcc-check-for-stack-protector-.patch
55f732c01e10948b283eb1cc77ce3f1169428251075f8f5dd3100d8fda1f08b6b3850969b84b42a52468d4f7c7ec7b49d01f0dff3c20c30f2e9ac0c07071b9d3  0001-x86-Compress-vmlinux-with-zstd-19-instead-of-22.patch
5464cf4b2e251972091cc9d5138291a94298737e98ec24829829203fc879593b440a7cd1ca08e6d01debaab2887976cb5d38fae2fbe5149cef1735d5d73bb086  0001-kexec-add-kexec_load_disabled-boot-option.patch
3816cfc8dd14d1c5ced05bbf19b099472c9bbbd1abced32cbb4b6b5baecfaa28c345e4e522648837d98e5bc7c311810cae9160f4c91019bb857a3857076b8e0e  awk.patch
07f678427e988c99314f2f983cbd3a90d3fa8578772e246c6e55ea3cfbcef4098e1413f5ad5f54a895f5d20c398d29ce2ccfc3c210a06e9e586f8280d0ef6e14  gcc13.patch
c74488940244ba032e741c370767467cfab93b89077b5dfccfed39b658f381e0995527e6c61de53b1c112b04ba647bfccf00f2e05c0637d3c703680a508821cc  ppc-notext.patch
f8582538a8482656138a0ae56b67a6d3cec09ff8d82a3f790ffa3fa7c37d0ae24a04f01f0ae1aec09838f86490907995c2568be7841a3821f0e64704f60153e5  0001-tty-Move-sysctl-setup-into-core-tty-logic.patch
0bfd2c138e997f25f821cd518263ad515fc303bbde37bcdb7dd651d30316c77652fec6fedb195102b8476b92b1e7269c143dad5df6431db49c4318375ad2e802  0002-tty-Allow-TIOCSTI-to-be-disabled.patch
8b245ec672a56dae7ce82f488880f6f3c0b65776177db663a29f26de752bf2cefe6b677e2cedbebd85e98d6f6e3d66b9f198a0d7a6042a20191b5ca8f4a749e7  0003-tty-Move-TIOCSTI-toggle-variable-before-kerndoc.patch
2f553ce374ac0be0c484e259d838507b6cba0ac46008f66add83966dece0734489cc14e35af402c2fd2ba25556ca0c4cec4fadb4d2374d242e7d9ae996daf36a  lts.aarch64.config
96eb129b984c68ae8534a56e4a395bf1c58deaacba6972150b821fac5819098fdd1847c4933b743ff40138f9e6a318ce91a6923b2bfcbfb231d546d37498827c  lts.armv7.config
06ac6cce4d56853c58f310371543668f8fcb9e0b183ae751e1945cb030761a78dbe582e6ababf44474c77fd022e1cb1fc1349bebd521573747518319991818c3  lts.x86.config
780bfa412fca20bcdbea6ab3fa671c7ecf2e206735f726ff6373bc042709911c85b5f06f16f08311281727e89785cf68da1c7f55f0f63115ae9b897c3b6f7b12  surface-lts.x86_64.config
f350c03e12614b2226f0416996bc9b32328bcb722690c82b5aadb893fda17edaf99d51bf298fbf4fb6a27bbc409055f59802de3fcffc7357c35aa8f80252b4b9  lts.ppc64le.config
9bfa5abbc186cecedc9b7da73f9deab8f8dc3c441ea29bdf5ca70c7417efb15f189ee2c1853db08d64db0d50fc417df9505833aa03b81e0078101fb634309a52  lts.s390x.config
70bb73b6b302e8276e335ecaf17049612cc02c789e3bbeb03ae4a759992fddd0e67a83b8d6963b6b763c0354617f458d982a6ad6f413f05d1e2d42c03fd5a626  virt.aarch64.config
d4323576ffeb7605346673f3f1bfe97bc8da8d00afc272ae6017ba2b5d6e4170004fddec155c993a85f66a3d0e9abeb7da95bbae1c1c9c6cc55235db7bc6d2d3  virt.armv7.config
4653f9d1a5cf2ffa469d06e3d6b123e9bca8faf3fd68fbd91cee87371455836b7bb83c8b65b697ad147422b8d3cedbb17d7f82b4d8d473062c76d629285feb64  virt.ppc64le.config
fe9b475b781f9fb579d3df4391e4d2e0c5055332d0aad4a63820d39ab62447f4eb41f6bd65b319c11fc60f5257b1b05b3be21b2bd9b4a150785c5c749395bd16  virt.x86.config
0b142e16c4a52ccf1572ddeb333a33d67a6dda46744e3ced18248e11fff4e5edcc1cf5173cbd6c9c3cf28e1dae56a36230ccefacd6b23c4cfc94a976a3803480  patch-6.1.43.xz
"
