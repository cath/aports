# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=alda
pkgver=2.2.5
pkgrel=0
pkgdesc="Music programming language for musicians"
url="https://github.com/alda-lang/alda"
arch="x86_64"
# aarch64: segfaults on gradle build
# everything else: blocked by openjdk17
license="EPL-2.0"
depends="openjdk17"
makedepends="go openjdk17-jdk gradle"
source="$pkgname-$pkgver.tar.gz::https://github.com/alda-lang/alda/archive/refs/tags/release-$pkgver.tar.gz
	alda-player
	"
builddir="$srcdir/$pkgname-release-$pkgver"
options="!check" # no tests provided by upstream

build() {
	(
		cd player
		gradle --no-daemon build
	)

	(
		cd client
		go generate
		go build
	)
}

package() {
	install -Dm755 client/client \
		"$pkgdir"/usr/bin/alda

	install -Dm644 player/build/libs/alda-player-fat.jar \
		-t "$pkgdir"/usr/share/alda/

	install -Dm755 "$srcdir"/alda-player \
		-t "$pkgdir"/usr/bin
}

sha512sums="
c554744466ad2f3fa2eb95389f2f65f38fca8f671eda3970dbe455920e55a28cf73d1f0fa68308a9efa5e0be7ff5821e8e93d47fd36c41f0ebd604d019c91000  alda-2.2.5.tar.gz
77d841e2b7873eb0de06723a5ea5373cc251ff8e82e1d542bc6e1fd297808e94bb9c322783366cbbea31f40284b073ed584296d02f30e36f9ac29158ad53430c  alda-player
"
