# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: Holger Jaekel <holger.jaekel@gmx.de>
pkgname=grass-gis
pkgver=8.3.0
_shortver=${pkgver%.*}; _shortver=${_shortver/./}
pkgrel=0
pkgdesc="Geographic Resources Analysis Support System"
url="https://grass.osgeo.org"
arch="all !s390x"  # s390x: tests fail
license="GPL-2.0-or-later"
depends_dev="
	freetype-dev
	gdal-dev
	libpq-dev
	proj-dev
	"
makedepends="
	$depends_dev
	bison
	bzip2-dev
	cairo-dev
	doxygen
	fftw-dev
	flex
	font-urw-base35
	geos-dev
	gettext-dev
	glu-dev
	gnutls-dev
	graphviz
	grep
	libjpeg-turbo-dev
	libpng-dev
	mariadb-connector-c-dev
	mesa-dev
	netcdf-dev
	openblas-dev
	opencl-dev
	openjpeg-dev
	pdal-dev
	py3-numpy-dev
	py3-wxpython
	readline-dev
	sqlite-dev
	tiff-dev
	unixodbc-dev
	zlib-dev
	zstd-dev
	"
checkdepends="bash py3-pytest"
depends="proj-util py3-six"
subpackages="$pkgname-dev-doc:devdoc:noarch $pkgname-dev $pkgname-doc $pkgname-gui::noarch $pkgname-lang"
langdir="/usr/lib/grass$_shortver/locale"
source="http://grass.osgeo.org/grass$_shortver/source/grass-$pkgver.tar.gz
	10-libintl.patch
	20-openblas.patch
	30-pkgconfig.patch
	40-int64.patch
	"
builddir="$srcdir/grass-$pkgver"

_use_pngquant=false
case "$CARCH" in
	s390x|riscv64) ;;
	*)
		makedepends="$makedepends pngquant"
		_use_pngquant=true
		;;
esac

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	# Ancient autoconf used upstream can't handle CPPFLAGS correctly, so set CPP to ignore warnings
	CPP="gcc -E -w" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr/lib \
		--enable-largefile \
		--with-cxx \
		--with-tiff \
		--with-libpng \
		--with-postgres \
		--with-postgres-includes=/usr/include/postgresql \
		--with-mysql \
		--with-mysql-includes=/usr/include/mysql/ \
		--with-sqlite \
		--with-opengl \
		--with-odbc \
		--with-fftw \
		--with-blas \
		--with-lapack \
		--with-cairo \
		--with-cairo-ldflags=-lfontconfig \
		--with-freetype \
		--with-nls \
		--with-readline \
		--without-opendwg \
		--with-regex \
		--with-pthread \
		--with-openmp \
		--with-opencl \
		--with-bzlib \
		--with-zstd \
		--with-gdal \
		--with-pdal \
		--with-netcdf \
		--with-geos \
		--with-x
	LC_ALL=C make

	# generate the Programmers' manual (in HTML)
	make htmldocs-single

	# save ~4mb of disk space by compressing PNG images (but takes a long time)
	if [ "$_use_pngquant" = true ]; then
		find lib/html -iname \*.png -print0 | xargs -0 -n 1 -P ${JOBS:-2} pngquant --speed 1 --force --ext .png
	fi
}

check() {
	export PYTHONPATH="$builddir/$(ls -d dist.*)/etc/python:$PYTHONPATH"
	export LD_LIBRARY_PATH="$builddir/$(ls -d dist.*)/lib:$LD_LIBRARY_PATH"
	export PATH="$builddir/$(ls -d bin.*):$PATH"

	.github/workflows/print_versions.sh
	.github/workflows/test_simple.sh
	pytest .

}

package() {
	DESTDIR="$pkgdir" make install INST_DIR=/usr/lib/grass$_shortver UNIX_BIN=/usr/bin

	# install pkg-config file
	mkdir -p "$pkgdir"/usr/share/pkgconfig
	install -m 644 grass.pc "$pkgdir"/usr/share/pkgconfig/grass$_shortver.pc

	# change section from 1 to .1grass
	for m in "$pkgdir"/usr/lib/grass"$_shortver"/docs/man/man1/*.1; do \
		n=$(basename $m .1); mv $m "$pkgdir"/usr/lib/grass$_shortver/docs/man/man1/$n.1grass;
		sed -i -e 's/^.TH \(.*\) 1/.TH \1 1grass/' "$pkgdir/usr/lib/grass$_shortver/docs/man/man1/$n.1grass"; done

	# escape minus signs which are command line options not hyphens
	for m in "$pkgdir"/usr/lib/grass"$_shortver"/docs/man/man1/*.1grass; do \
		sed -i -e 's/\([ ([]\)-\([a-z]\)/\1\\-\2/g' \
			-e 's/\([ []\)--\([a-z]\)/\1\\-\\-\2/g' \
			-e 's/\[-\\fB/[\\-\\fB/' \
			-e 's/\[--\\fB/[\\-\\-\\fB/g' \
			-e 's/"\\fB-\([a-zA-Z0-9]\)/"\\fB\\-\1/' \
			-e 's/"\\fB--\([a-zA-Z0-9]\)/"\\fB\\-\\-\1/' \
			-e 's/\\fI-\([a-zA-Z0-9]\)/\\fI\-\1/g' \
			"$m"; done

	# move manpages to /usr/share/man
	mv "$pkgdir"/usr/lib/grass$_shortver/docs/man "$pkgdir"/usr/share

	# move docs to /usr/share/doc
	mkdir -p "$pkgdir/usr/share/doc"
	mv "$pkgdir/usr/lib/grass$_shortver/docs" "$pkgdir/usr/share/doc/grass-doc"

	# move programming-manual to /usr/share/doc
	mkdir -p "$pkgdir/usr/share/doc/grass-dev-doc/programming-manual"
	mv "$builddir/lib/html" "$pkgdir/usr/share/doc/grass-dev-doc/programming-manual"

	# install icons and desktop file
	mkdir -p "$pkgdir"/usr/share/icons
	mv "$pkgdir"/usr/lib/grass$_shortver/share/icons/hicolor "$pkgdir"/usr/share/icons

	mkdir -p "$pkgdir"/usr/share/applications
	mv "$pkgdir"/usr/lib/grass$_shortver/share/applications/grass.desktop "$pkgdir"/usr/share/applications/grass$_shortver.desktop

	# install AppStream metadata
	mkdir -p "$pkgdir"/usr/share/metainfo
	mv "$pkgdir"/usr/lib/grass$_shortver/share/metainfo/org.osgeo.grass.appdata.xml "$pkgdir"/usr/share/metainfo/org.osgeo.grass.appdata.xml

	# Remove empty directory
	rmdir "$pkgdir"/usr/lib/grass$_shortver/gui/wxpython/scripts/

	# Remove files not installed
	rm -rf 	"$pkgdir"/usr/lib/grass$_shortver/INSTALL.md \
		"$pkgdir"/usr/lib/grass$_shortver/REQUIREMENTS.md \
		"$pkgdir"/usr/lib/grass$_shortver/demolocation/PERMANENT/.tmp/* \
		"$pkgdir"/usr/lib/grass$_shortver/translation_status.json \
		"$pkgdir"/usr/lib/grass$_shortver/translators.csv

	# Move image files from /usr/lib/grass$_shortver to /usr/share/grass$_shortver
	mkdir -p "$pkgdir"/usr/share/grass$_shortver/gui
	mv "$pkgdir"/usr/lib/grass$_shortver/gui/icons/  "$pkgdir"/usr/share/grass$_shortver/gui/
	mv "$pkgdir"/usr/lib/grass$_shortver/gui/images/ "$pkgdir"/usr/share/grass$_shortver/gui/

	find "$pkgdir"/usr/lib/grass$_shortver/gui/wxpython/ -type f \( -name "*.jpg" -or -name "*.png" \) -print | sort > "$builddir"/grass-gui.image-file-in-usr-lib.list

	while read -r file; do \
		dir=$(dirname "$file" | sed 's/usr\/lib\//usr\/share\//') ; \
		if [ ! -e "$dir" ]; then \
			mkdir -p "$dir" ; \
		fi ; \
		mv "$file" "$dir" ; \
	done < "$builddir"/grass-gui.image-file-in-usr-lib.list

	# Remove empty files
	find "$pkgdir" -type f -empty -name "class_graphical*" -print -delete
}

devdoc() {
	pkgdesc="$pkgdesc (development documentation)"

	amove usr/share/doc/grass-dev-doc
}


dev() {
	default_dev

	# move *.so links from usr/lib/grass$_shortver/lib
	# needed when linking the apps to -dev packages
	for i in usr/lib/grass"$_shortver"/lib/*.so; do
		if [ -L "$i" ]; then
			amove "$i"
		fi
	done
}

doc() {
	default_doc

	mkdir -p "$subpkgdir/usr/lib/grass$_shortver"
	ln -s ../../share/doc/grass-doc "$subpkgdir/usr/lib/grass$_shortver/docs"
}

gui() {
	pkgdesc="$pkgdesc (graphical user interface)"
	depends="$pkgname=$pkgver-r$pkgrel  ghostscript py3-matplotlib py3-opengl py3-pillow py3-wxpython"

	amove usr/lib/grass$_shortver/gui
	amove usr/share/grass$_shortver/gui
	ln -s ../../../share/grass$_shortver/gui/icons "$subpkgdir/usr/lib/grass$_shortver/gui/icons"
	ln -s ../../../share/grass$_shortver/gui/images "$subpkgdir/usr/lib/grass$_shortver/gui/images"
}

sha512sums="
0d6c0a9ec7038cf707f868144aec3fb4c59c72c56b9cff4c7b2f256c90cbd7e45c1851a7f7a37e7b9ac42f6bbaecb4e8fa4ad7d5eb0f88adf9c2b3bfa23ecf15  grass-8.3.0.tar.gz
900c9646ab1db27b196ddd9d5e8ab7419817f73f578b671234a766b68533adde7ab9f8940feb0e1ed42d873b6411c90f45c841e446ee63d5e56806374d7bcb3e  10-libintl.patch
b7ef74dd185e80262bbbc8eb4717438defebc7e61c39e06b4e6d4dce96b80ae40cdbd021315821ca76ffc3dc981de72ef062221b74157d0334dd66e3d1e610cf  20-openblas.patch
764f0169172ab6683f9a4b9ab2e4354a7aa19d722acc1663fe007a4eda593afb7dee53b23c21e2daf0d37c038796473a65faab89dd0a00f630f5c7cfbf3e67cc  30-pkgconfig.patch
2de663f5826e861552ef501712772467f1fd2d04e0ad0a5882c8f9c52d1617c17af4bd74ea23772c9df0203f27e2f3d57882d505206e9f662002758cf1baad00  40-int64.patch
"
