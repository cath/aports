# Maintainer:
pkgname=uhd
pkgver=4.4.0.0
pkgrel=2
pkgdesc="USRP Hardware Driver"
url="https://github.com/EttusResearch/uhd"
options="!check" # no tests
arch="all !armhf !s390x" # build failure
license="GPL-3.0-or-later"
makedepends="
	boost-dev
	cmake
	libusb-dev
	py3-mako
	python3-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/EttusResearch/uhd/archive/refs/tags/v$pkgver.tar.gz
	uhdlib-include-stdint.patch
	"

build() {
	case "$CARCH" in
	aarch64)
		local neon=ON
		;;
	*)
		local neon=OFF
		;;
	esac
	cmake -B build-host -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DNEON_SIMD_ENABLE=$neon \
		host
	cmake --build build-host
	sed -i "s|Version:.*|Version: $pkgver|" build-host/uhd.pc
}

package() {
	DESTDIR="$pkgdir" cmake --install build-host
}

sha512sums="
3c9b57dc776e0fadc991ffeede84c2367f7403bfae6fd30a045baf1c9cfb21889310b9a8340a35ea13eea00208bf678cccd7b82e190dfb6b9d61e42bdef3b21a  uhd-4.4.0.0.tar.gz
545747ca3a8a812438339337ed5be0596d014c965264d0afd7da5729eb98459710df718f8aac346b690abac2e1258b2e0ca8d99ab5b817eea3c95d6a6c95b873  uhdlib-include-stdint.patch
"
