# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pulumi-language-java
pkgver=0.9.5
pkgrel=0
pkgdesc="Infrastructure as Code SDK (Java language provider)"
url="https://pulumi.com/"
# blocked by pulumi
arch="x86_64 aarch64"
license="Apache-2.0"
depends="pulumi"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi-java/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/pulumi-java-$pkgver"
# Tests require schemas from pulumi repository and gradle
options="!check"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	local _goldflags="-X github.com/pulumi/pulumi-java/pkg/version.Version=v$pkgver"
	mkdir "$builddir"/bin

	cd "$builddir"/pkg
	go build -v \
		-o ../bin/pulumi-language-java \
		-ldflags "$_goldflags" \
		./cmd/pulumi-language-java
	go build -v \
		-o ../bin/pulumi-java-gen \
		-ldflags "$_goldflags" \
		./cmd/pulumi-java-gen

	cd "$builddir"
	./bin/pulumi-java-gen completion bash > pulumi-java-gen.bash
	./bin/pulumi-java-gen completion fish > pulumi-java-gen.fish
	./bin/pulumi-java-gen completion zsh > pulumi-java-gen.zsh
}

package() {
	install -Dm755 bin/pulumi-language-java -t "$pkgdir"/usr/bin/
	install -Dm755 bin/pulumi-java-gen -t "$pkgdir"/usr/bin/

	install -Dm644 pulumi-java-gen.bash \
		"$pkgdir"/usr/share/bash-completion/completions/pulumi-java-gen
	install -Dm644 pulumi-java-gen.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/pulumi-java-gen.fish
	install -Dm644 pulumi-java-gen.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_pulumi-java-gen
}

sha512sums="
2b9c65c17cb0d1deb5bc9fe26fa76b83e483625201a42683e7fa4786337feb03355b38a6fa0ad6372ea52f281b501f808ce315a806b7873d4cfd49d54c51f6ef  pulumi-language-java-0.9.5.tar.gz
"
