# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=repgrep
_pkgname=rgr
pkgver=0.14.2
pkgrel=0
pkgdesc="Interactive command line replacer for ripgrep"
url="https://github.com/acheronfail/repgrep"
arch="all"
license="MIT OR Apache-2.0 OR Unlicense"
depends="ripgrep"
makedepends="
	cargo
	cargo-auditable
	asciidoctor
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/acheronfail/repgrep/archive/$pkgver.tar.gz"
options="net"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 "target/release/$_pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE-MIT -t "$pkgdir/usr/share/licenses/$pkgname"
	out_dir=$(find target -name repgrep-stamp -print0 | xargs -0 ls -t | head -n1 | xargs dirname)
	install -Dm 644 "$out_dir/$_pkgname.bash" "$pkgdir/usr/share/bash-completion/completions/$_pkgname"
	install -Dm 644 "$out_dir/$_pkgname.fish" "$pkgdir/usr/share/fish/vendor_completions.d/$_pkgname.fish"
	install -Dm 644 "$out_dir/_$_pkgname" "$pkgdir/usr/share/zsh/site-functions/_$_pkgname"
	install -Dm 644 "$out_dir/$_pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
ba274eb234ddb99d1c9f7526bec4e06ca76ef899f2b600e3e1e9601893bbbd223c346b863503692173bf1f8d2666fef88085db76fb0321e5fc462a51015355ec  repgrep-0.14.2.tar.gz
"
