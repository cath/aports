# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=mautrix-whatsapp
pkgver=0.9.0
pkgrel=0
pkgdesc="Matrix-WhatsApp puppeting bridge"
url="https://maunium.net/go/mautrix-whatsapp"
arch="all !s390x"
license="AGPL-3.0-or-later"
makedepends="go olm-dev sqlite-dev"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="$pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://github.com/mautrix/whatsapp/archive/v$pkgver.tar.gz
	mautrix-whatsapp.initd
	mautrix-whatsapp.confd
	default-log-dir.patch
	"
builddir="$srcdir"/whatsapp-$pkgver

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	export CGO_CFLAGS="$CFLAGS"
	export CGO_LDFLAGS="$LDFLAGS"
	go build
}

check() {
	go test -v ./...
}

package() {
	install -Dm755 mautrix-whatsapp "$pkgdir"/usr/bin/mautrix-whatsapp
	install -Dm644 example-config.yaml "$pkgdir"/etc/mautrix-whatsapp/example-config.yaml
	install -Dm755 "$srcdir"/mautrix-whatsapp.initd "$pkgdir"/etc/init.d/mautrix-whatsapp
	install -Dm644 "$srcdir"/mautrix-whatsapp.confd "$pkgdir"/etc/conf.d/mautrix-whatsapp
}

sha512sums="
1e788c0949dd5c4df6991c6d2f6db208f832358ce33389d4d14ea911a5fef7b6badc04cf4f4854739c9f0773242c29b5b179324b89f07136b1be7b5accbfc31c  mautrix-whatsapp-0.9.0.tar.gz
c735950faace99380a5cfd998ede482108381fcb48bdcd2405d32a4a84e12b2ee75995c3c12160d257479612dcd0ea4c773d4d7b6bef6f6bbbd0373f31f20acd  mautrix-whatsapp.initd
9349b660273c63d2973f1b99ddbd98469dddc098157380603210159f17d3cb1eb55e71dbd21550b20d40831f4da320225e7c03441667e2750e30a2e1fa03acfe  mautrix-whatsapp.confd
d5b66b40dde25f24986788669b91dfafe2f35f901b87ff30bfe8c8ab663ebbbe9bd10ac974f73115abd935eb0f27d5645fcbe641a6c6925c24eaa8c7fa9ab6b7  default-log-dir.patch
"
